package com.sda.alexandra;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.printf("Numele meu este %s si prenumele este %s", "Pupaza", "Alexandra"
                + "\n");//%s se inlocuieste cu este dupa ,
        printName();
        printTable();
        printTableWithStringFormat();
        printFormattedNumber();
        printNumberTriangle(10);
        printNumberTriangle(readNumberFromConsole());
        printNumberTriangleSecondMethod(10);
    }


    public static void printName() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduceti numele de familie: ");
        String nume = scanner.nextLine();
        System.out.println("Introduceti prenumele: ");
        String prenume = scanner.nextLine();
        System.out.printf("Numele de familie este %s si prenumele este %s", nume, prenume + "\n");
        // System.out.printf("Numele de familie este %10s si prenumele este %-10s", nume, prenume+"\n");//%10s inlocuieste cu exact 10 caractere, adauga caractere in pus daca nu avem 10 caractere sau nu adauga nimic daca deja avem
        //%10s inainte de cuv nostru, %-10 dupa cuv nostru
        System.out.println();

    }

    public static void printTable() {
        System.out.printf("%-20s %14s", "Exam Name", "Exam Grade" + "\n");
        System.out.println("----------------------------------------------------");
        System.out.printf("%-15s %10s", "Java", "A" + "\n");
        System.out.printf("%-15s %10s", "PHP", "B" + "\n");
        System.out.printf("%-15s %10s", "VB NET", "A" + "\n");
        System.out.println("----------------------------------------------------");

    }

    private static void printTableWithStringFormat() {
        System.out.println(String.format("%-20s %s", "Exam Name", "Exam Grade"));
        System.out.println("-------------------------------------------");
        System.out.println(String.format("%-24s %s", "Java", "A"));
        System.out.println(String.format("%-24s %s", "Php", "B"));
        System.out.println(String.format("%-24s %s", "VB net", "A"));
        System.out.println("-------------------------------------------");
    }

    public static void printFormattedNumber() {
        // System.out.printf("Numarul %.1f este formatat", 7.9 + "\n");
        System.out.printf("Numarul %10.1f este formatat", 7.9);
        System.out.printf("Numarul %-11.1f este formatat \n", 7.99);
    }

    public static int printNumberTriangle(int number) {

        for (int i = 1; i <= number; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j + " ");
            }
            System.out.println();
        }
        return 0;
    }

    public static void printNumberTriangleSecondMethod(int n) {
        String text = "";
        for (int i = 1; i <= n; i++) {
            text = text + i + " ";
            System.out.println(text);
        }
    }

    public static int readNumberFromConsole() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introdu numar: ");
        return scanner.nextInt();
    }

}

