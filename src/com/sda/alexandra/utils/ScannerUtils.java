package com.sda.alexandra.utils;

import java.util.Scanner;

public class ScannerUtils {

    private static Scanner scanner;

    public static int getNumberFromInput() {
        Scanner scanner = getScanner();
        int userNumberInput;
        try{

            userNumberInput=scanner.nextInt();

        }catch (Exception ex){

            System.out.println("A intrat exceptia, introdu un numar valid!");
            scanner.nextLine();
            userNumberInput=getNumberFromInput();//metoda recursiva, ca sa nu o iau de la capat cu verificarea, metoda este apelata din interiorul ei se apeleaza pe ea insasi si face din nou acelasi lucru

        }

        return userNumberInput;

    }
    public static float getNumberFromInputFloat() {
        Scanner scanner = getScanner();
        float userNumberInput;
        try{

            userNumberInput=scanner.nextFloat();

        }catch (Exception ex){

            System.out.println("A intrat exceptia, introdu un numar valid!");
            scanner.nextLine();
            userNumberInput=getNumberFromInputFloat();//metoda recursiva, ca sa nu o iau de la capat cu verificarea, metoda este apelata din interiorul ei se apeleaza pe ea insasi si face din nou acelasi lucru

        }

        return userNumberInput;

    }

    public static void getPerimeterOfACircle() {
        System.out.println("Introdu diametrul cercului: ");
        float diameter= ScannerUtils.getNumberFromInputFloat();//diametru=2R/ perimetrul=2*PI*R sau Pi*Diameter
        System.out.println("Perimetrul cercului este: "+ Math.PI*diameter);


    }

    public static Scanner getScanner() {
        if (scanner == null) {
            scanner = new Scanner(System.in);
        }
        return scanner;
    }
}
